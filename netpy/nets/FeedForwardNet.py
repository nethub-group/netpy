from netpy.nets.net import Net

class FeedForwardNet(Net):

    """ Class for Feed Forward Net  """

    def __init__(self, name):
        super().__init__(name)
